# Bonjour, Je suis Samuel

Je suis actuellement TechLead chez [Dataraxys](https://www.dataraxys.com/) éditeur de la
solution [Nuamouv](https://www.nuamouv.com/).

Nuamouv est une solution de billettique légère. Au cœur de cette solution, la géolocalisation du véhicule et un
équipement nomade (lecteur NFC, smartphone/tablette et imprimante thermique). Nuamouv aide les entreprises de transport
de voyageurs dans le suivie d'exploitation et la commercialisation à bord.

Dataraxys mets également à disposition des voyageurs des outils d'informations en temps réel et d'achat en ligne de leur
titre de transport.

Je suis également administrateur de [LorraineJug](https://www.twitter.com/LorraineJug), association qui organise des
soirées de conférences autour de la plateforme Java et des technologies associées.

Précédement, j'était Developpeur Senior et Expert Java chez [Sopra Steria](https://www.soprasteria.fr/). Au cours de
différentes missions, j'ai pû intervenir dans des domaines aussi variés que la grande distribution, la logistique,
l'éducation, la protection sociale. J'était également coordinateur des relations avec les écoles et chargé de
recrutement.

J'ai également accompagné [Ressources Informatiques](https://www.nehs-digital.com/) dans la conception de la nouvelle
version du RIS Radio 3000 en temps qu'Architecte .NET Junior.

## Social

Où me suivre 👋

- [@sdalichampt on Gitlab](https://gitlab.com/sdalichampt)
- [@s.dalichampt.fr on Bluesky](https://bsky.app/profile/s.dalichampt.fr)
- ~~[@sdalichampt on Twitter](https://twitter.com/sdalichampt)~~
- [@samueldalichampt on LinkedIn](https://www.linkedin.com/in/samueldalichampt/)
- [Site personnel www.dalichampt.fr](https://www.dalichampt.fr/)
